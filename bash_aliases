# Built-ins
# change to home directory: cd
# change to last directory: cd -

# Pushing changes the directory, Popping does not, but removes the item from the stack
alias ?='dirs -v'  # print the stack, also use plain dirs
alias p='pushd'
alias o='popd'

