#Readme

##devbox 
equivalent box running on a test or stage environment is presumably the testbox, stagebox, prodbox...

###Build 

$ docker build -t "jawford/devbox"

###Run 
On a Mac, with Boot2docker, /Users is available

$ docker run -it --rm \
             -v /Users:/Users \
             -v /var/run/docker.sock:/var/run/docker.sock \
             -v $(which docker):/bin/docker \
             --name "devbox" \
             "jawford/devbox:latest"

